#!/bin/bash

### SCRIPT TO BACKUP SITE DBS AND SITE FOLDERS ON UBUNTU SERVERS TO NAS DEVICE ###
### By Elia Nazarenko, Student Affairs Systems Group, University of Arizona July 2013 ###

# MUSER is the root MySQL logon, leave as root #
# MPASS CHANGE THIS, is the root MySQL password #
# MHOST is where the MySQL DB is hosted, leave as local host, don't change #
# MYSQL don't change this field #
# MYSQLDUMP don't change this field #
# BAK temporary working backup directory for script, don't change #
# GZIP don't change this field #
# RSYNC don't change, this defines the path to this program #
# SSH don't change, this defines the path to this program #
# KEY CHANGE THIS, this path has to point the the location of your generated private key, be sure to change both the username and key file name as appropriate to the webserver #
# RUSER this is the remote user on the NAS device, don't change #
# RHOST this is the IP of the NAS device #
# LHOST don't change this is a temporary local directory for zipped backup files sites and database dump #
# RPATH this is the remote path on the NAS for keeping backups, don't change #
# HOSTNAME CHANGE THIS, update this to reflect the server's name, this will be user for naming your files #
# NOW defines how the data format looks, do not change #

### MySQL Server Login Info ###
MUSER="root"
MPASS="{{ mysql_root_pass }}"
MHOST="localhost"
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
BAK="/backup/mysql"
GZIP="$(which gzip)"

### SSH SERVER Login info ###
RSYNC=/usr/bin/rsync
SSH=/usr/bin/ssh
KEY=/home/ansible-admin/.ssh/{{ inventory_hostname }}
RUSER="linuxcopy"
RHOST="172.28.146.121"
LPATH="/backup"
RPATH="/volume1/homes/linuxcopy/drupalbackups"
HOSTNAME="{{ inventory_hostname }}"
NOW=$(date +"%Y-%m-%d")

### This part of the script looks for the temporary working backup directory and creates it if it's not already there ### [ ! -d $BAK ] && mkdir -p $BAK || /bin/rm -f
### $BAK/* ###
[ ! -d "$BAK" ] && mkdir -p "$BAK"

### This part of the script, lists all the DBs the server has in MySQL ###

DBS="$($MYSQL -u $MUSER -h $MHOST -p$MPASS -Bse 'show databases')"

echo "Launching backup script at $(date)"


### This part of the script goes through all the DBs listed and dumps each of them to a file ###

for db in $DBS
do
 FILE=$BAK/$db.$NOW-$(date +"%T").gz
 $MYSQLDUMP -u $MUSER -h $MHOST -p$MPASS $db | $GZIP -9 > $FILE
done


### This part of the script zips up the site folder and also zips up all the databases into one gzip file ###

tar -zcf /backup/$HOSTNAME.$NOW.mysql.tgz /backup/mysql
tar -zcf /backup/$HOSTNAME.$NOW.sites.tgz /var/www/web


### Removes the temporary working directory after gzip ###

sudo rm -R /backup/mysql


### Initiates passwordless SSH connection with RSYNC and pushes backup files to the NAS ###

$RSYNC -az -e "$SSH -i $KEY" $LPATH $RUSER@$RHOST:$RPATH


### Deletes temporary directory with local copies of the backup after upload ###

sudo rm -R /backup

echo "Backup script completed execution at $(date)"
